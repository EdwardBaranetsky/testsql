
/* вывести список заказов вместе с количеством товаров в данных заказах:*/

SELECT o.order_number, count(op.product_id) as product_count
FROM orders o
INNER JOIN orders_products op ON op.order_id = o.id
GROUP BY o.order_number;

-----------------------------------------------------------------------
/* вывести все заказы, в которых больше 10 товаров:*/

SELECT order_number
FROM orders o
INNER JOIN orders_products op ON op.order_id = o.id
GROUP BY o.order_number
HAVING count(op.product_id) > 10;

-----------------------------------------------------------------------
/* вывести два любых заказа, у которых максимальное количество общих товаров: */

WITH topOrders as (SELECT op2.order_id fid, op1.order_id sid
FROM orders_products op1
INNER JOIN orders_products op2 ON op2.product_id = op1.order_id
AND op2.order_id <> op1.order_id
GROUP BY op2.order_id, op1.order_id
ORDER BY count(*) DESC
LIMIT 1)
SELECT order_number
FROM orders
WHERE id = (SELECT fid FROM topOrders LIMIT 1) or id = (SELECT sid FROM topOrders LIMIT 1);
/* тут можно было обойтись выборкой topOrders, смотря как нужно вывести*/
-----------------------------------------------------------------------